# PROJECT NAME

## Overview of the Project
Describes the scope of this project

## API documentationm
a link that directs the user to the API documentation if this is a component requires API access

## Requirements (optional)
If users have to install anything or pre-download and configuration, this is a good place to include that information.

## Configurations
List out all the required parameters for the project, essentially what the user will be seeing in the UI.
Example:
    1. API TOKEN
        - a guideline for user if needed. Some sources are a pain to fetch what is required for the extraction. If some parameters are optional, it will be a good time to explain how the extractor will behave if the parameters are missing.

## RAW JSON Configuration (if exist)
This is basically what the UI will return you when the user enter the required information. This is my personally preference. It helps me to refresh my memory on how i was configuring the extractor when i had to re-develope the code after months of completing the code. My best example is my Quickbooks Extractor.


Your Name
Contact info